<?php

declare(strict_types=1);

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Security\Core\User\UserInterface;

/** @ODM\Document(db="test_database", collection="users") */
class User implements UserInterface
{
    /** @ODM\Id(strategy="UUID", type="string") */
    protected $id;

    /** @ODM\Field(type="string") */
    public $name;

    /** @ODM\Field(type="string") */
    public $salt;

    /** @ODM\Field(type="string") */
    public $password;

    /** @ODM\Field(type="string") */
    public $role;

    /** @ODM\Field(type="bool") */
    public $active;

    public function __construct(string $name = '', bool $active = true)
    {
        $this->name = $name;
        $this->active = $active;
    }

    public function getRoles(): array
    {
        return [$this->role];
    }

    public function getPassword():? string
    {
        return $this->password;
    }

    public function getSalt():? string
    {
        return $this->salt;
    }

    public function getUsername(): string
    {
        return $this->name;
    }

    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt): void
    {
        $this->salt = $salt;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}