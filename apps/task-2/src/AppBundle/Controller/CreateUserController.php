<?php

declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Document\User;
use AppBundle\Form\NewUserForm;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserController extends Controller
{
    /**
     * @Route("/", name="create_user")
     */
    public function createUserAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, DocumentManager $dm)
    {
        $user = new User();
        $form = $this->createForm(NewUserForm::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setRole('ROLE_USER');
            $user->setActive(true);
            $dm->persist($user);
            $dm->flush();
            return $this->redirectToRoute('users');
        }

        return $this->render(
            'user/new_user_form.html.twig',
            ['form' => $form->createView()]
        );
    }
}