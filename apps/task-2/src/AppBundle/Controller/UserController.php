<?php

declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    /**
     * @Route("/users", name="users")
     * @Route("/users/{page}", name="users_paged")
     */
    public function getUsersAction(DocumentManager $dm, int $page = 1): Response
    {
        $totalCount = $dm->createQueryBuilder(User::class)
            ->count()->getQuery()->execute();
        $packSize = $this->getParameter('default_pack_size');
        $maxPack = ceil($totalCount/$packSize);
        $page = $page > $maxPack ? $maxPack : $page;
        $skip = ($page-1) * $packSize;
        $users = $dm->createQueryBuilder(User::class)
            ->limit($packSize)
            ->skip($skip)
            ->getQuery()
            ->execute();

        return $this->render('user/list.html.twig', [
            'users' => $users,
            'currentPage' => $page,
            'packSize' => $packSize,
            'maxPage' => $maxPack,
        ]);
    }

    /**
     * @Route("/users/{id}/disable", name="user_disable", requirements={"id"="^[a-zA-Z0-9]*$"})
     */
    public function disableUserAction(DocumentManager $dm, Request $request, string $id): Response
    {
        return $this->changeUserStatus($dm, $request, $id, false);
    }

    /**
     * @Route("/users/{id}/enable", name="user_enable", requirements={"id"="^[a-zA-Z0-9]*$"})
     */
    public function enableUserAction(DocumentManager $dm, Request $request, string $id): Response
    {
        return $this->changeUserStatus($dm, $request, $id, true);
    }

    private function changeUserStatus(DocumentManager $dm, Request $request, string $id, bool $active): RedirectResponse
    {
        /** @var User $user */
        $user = $dm->getRepository(User::class)->find($id);
        if (!$user) {
            throw $this->createNotFoundException('User not found for id '.$id);
        }

        if ($active === $user->isActive()) {
            return $this->redirectToRoute('users');
        }

        $user->setActive($active);
        $dm->flush();
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }
}