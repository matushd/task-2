<?php 

class UsersListCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/login');
        $I->submitForm('form', [
            '_username' => 'rrrrrrrr',
            '_password' => 'rrrrrrrr'
        ]);
        $I->amOnPage('/users');
    }

    // tests
    public function paginationTest(AcceptanceTester $I)
    {

        $I->click('Next');
        $I->amOnPage('/users/2');
        $I->click('19');
        $I->amOnPage('/users/19');
        $I->click('Previous');
        $I->amOnPage('/users/18');
    }

    // tests
    public function switchUserStatusTest(AcceptanceTester $I)
    {
        $I->click('disable');
        $I->amOnPage('/users/1');
        $I->see('not active');
        $I->click('enable');
        $I->amOnPage('/users/1');
        $I->see('active');
    }
}
