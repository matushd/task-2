<?php 

class SigninCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('Sign in');
    }

    // tests
    public function successLoginTest(AcceptanceTester $I)
    {
        $I->submitForm('form', [
            '_username' => 'rrrrrrrr',
            '_password' => 'rrrrrrrr'
        ]);
        $I->seeInCurrentUrl('/users');
    }

    public function failedLoginTest(AcceptanceTester $I)
    {
        $I->submitForm('form', [
            '_username' => 'rrrrrrrr',
            '_password' => 'sssssss'
        ]);
        $I->seeInCurrentUrl('/login');
        $I->see('Invalid credentials.');
    }

    // tests
    public function successLogoutTest(AcceptanceTester $I)
    {
        $I->submitForm('form', [
            '_username' => 'rrrrrrrr',
            '_password' => 'rrrrrrrr'
        ]);
        $I->seeInCurrentUrl('/users');
        $I->click('Logout');
        $I->seeInCurrentUrl('/');
    }
}
