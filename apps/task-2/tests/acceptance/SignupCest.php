<?php 

class SignupCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function successSignUpTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('Name');
        $I->click('Sign up');
        $I->submitForm('form', [
            'new_user_form[name]' => 'rrrrrrrr',
            'new_user_form[password][first]' => 'rrrrrrrr',
            'new_user_form[password][second]' => 'rrrrrrrr',
        ]);
        $I->seeInCurrentUrl('/login');
    }

    public function failedSignUpTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('Name');
        $I->click('Sign up');
        $I->submitForm('form', [
            'new_user_form[name]' => 'rrrrrrrr',
            'new_user_form[password][first]' => 'rrsssrrrrrr',
            'new_user_form[password][second]' => 'rrrrraaarrr',
        ]);
        $I->seeInCurrentUrl('/');
        $I->see('This value is not valid.');
    }
}
