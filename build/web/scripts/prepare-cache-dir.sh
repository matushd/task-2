#!/usr/bin/env bash
echo "Prepare cache dir ..."
chown -R www-data:www-data /web/var
exec "$@"
