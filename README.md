# Task 2

## User Story 1:
As a user, I want to see an option to either sign in or sign up, so I can access application with my account

Acceptance criteria:

* User should see sign in screen with input data fields for 1) Username 2) Password
* User should see an option to Sign-up if they don't have an account created - clicking on this option will route user to the Sign-in screen
* User with disabled account should not sign in
* Signed in User should be redirect to restricted part of application
* Signed in User should see logout option
 
## User Story 2:
As a signed in User, I want to see paginated list of all application users, where I can disable each application account.

Acceptance criteria

* User should see paginated list with maximum 10 rows with columns 1) username and 2) action
* User should see disable button which is active for enabled users, and disabled for disabled users.
* User should be able to paginate over the list.

## Init
### Clone repo
```
git clone https://matushd@bitbucket.org/matushd/task-2.git
```
### Build containers
```
docker-composer up
```

### Run website
```
localhost:8000
```

### Acceptance tests
```
docker-compose run php bash
cd /web
php vendor/bin/codecept run acceptance
```